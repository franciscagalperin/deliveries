class ShipmentsController < ApplicationController
  def test
    render :json => {
        "name": "Fran",
        "last_name": "Galperin"
    }
  end

  def create
    # create the shipment to work with.
    @shipment = Shipment.new
    status = "ERROR"
    if params[:shipment]

      @shipment.sender = params[:shipment][:sender]
      @shipment.receiver = params[:shipment][:receiver]
      @shipment.from_lat = (params[:shipment][:from_lat])
      @shipment.from_long = (params[:shipment][:from_long])
      @shipment.to_lat = (params[:shipment][:to_lat])
      @shipment.to_long = (params[:shipment][:to_long])
      @shipment.price = @shipment.calculatePrice
      @shipment.delivery = (params[:shipment][:delivery])
      @shipment.status = Shipment::STATUS[:IN_COURSE]
      @shipment.date_created = DateTime.now


      if @shipment.save
        status = "OK"
      else
        status = "NOT_SAVED"
      end

      render :json =>{
          :data => @shipment,
          :status => status
      }
    else
      render json: status
    end

  end

end
