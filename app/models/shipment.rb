class Shipment < ApplicationRecord

  STATUS = {:IN_COURSE => "En curso", :DELIVERED => "Entregado"}

  # needed for upload images, this maps the field with the handler
  # mount_uploader :signature, PictureUploader

  # validations for the Shipment's model
  validates :from_lat, presence: true
  validates :from_long, presence: true
  validates :to_lat, presence: true
  validates :to_long, presence: true
  validates :sender, presence: true
  validates :receiver, presence: true

  ### Here you can place your price logic that will consume the price for an external api
  def calculatePrice
    # at this point we only return a random value between 100 and 1000
    price = Random.new
    return price.rand(100...1000)
  end


end
