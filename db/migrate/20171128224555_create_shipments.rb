class CreateShipments < ActiveRecord::Migration[5.1]
  def change
    create_table :shipments do |t|
      t.integer :sender
      t.integer :receiver
      t.float :from_lat
      t.float :from_long
      t.float :to_lat
      t.float :to_long
      t.integer :price
      t.string :status
      t.integer :delivery
      t.string :signature
      t.datetime :date_created
      t.datetime :date_delivered
    end
  end
end
